package ru.avvolnov.bigdata.task1;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;

import java.io.IOException;

public class Reducer extends org.apache.hadoop.mapreduce.Reducer<LongWritable, LongWritable, Text, LongWritable> {
    private Text currentBrowserName = new Text();
    private LongWritable currentBrowserUserCount = new LongWritable();

    @Override public void reduce(LongWritable key, Iterable<LongWritable> values, Context context)
            throws IOException, InterruptedException {
        long count = 0;
        long curIP = -1;
        for(LongWritable val: values) {
            long value = val.get();
            if(value == curIP) continue;
            count++;
            curIP = value;
        }
        currentBrowserName.set(MapUtils.browserFromCompositeKey(key.get()).toString());
        currentBrowserUserCount.set(count);
        context.write(currentBrowserName, currentBrowserUserCount);
    }
}
