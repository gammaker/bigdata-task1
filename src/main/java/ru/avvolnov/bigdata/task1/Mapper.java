package ru.avvolnov.bigdata.task1;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;

import java.io.IOException;

// Mapper отображает каждую строку в пару ключ-значение,
// где ключ состоит из адреса и браузера пользователя, а значение равно адресу пользователя.
// На последующем этапе эти пары будут сгруппированы по браузеру, а все значения будут идти в порядке возрастания адресов.
public class Mapper extends org.apache.hadoop.mapreduce.Mapper<Object, Text, LongWritable, LongWritable> {
    private final LongWritable outputKey = new LongWritable();
    private final LongWritable outputValue = new LongWritable();

    // Упрощённая хеш таблица. Позволяет убрать большую часть дубликатов пар на стадии Map.
    private long[] keyCache = new long[509];

    private int toCacheIndex(long compositeKey) {
        int result = (int)(compositeKey % keyCache.length);
        if(result < 0) result += keyCache.length;
        return result;
    }

    @Override public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        final String line = value.toString();
        final long ip = MapUtils.logLineToLongIP(line);
        final BrowserType browser = MapUtils.logLineToBrowser(line);
        final long compositeKey = MapUtils.makeCompositeKey(ip, browser.ordinal());

        // Если строка в логе от того же самого пользователя с тем же браузером уже была, её можно отбросить.
        // При этом используем кеш ограниченного размера, который никогда не приведёт к нехватке памяти.
        // Этот метод не даёт 100% гарантии удаления дубликатов, и используется
        // только для сокращения записи на диск и передачи по сети.
        // Оставшиеся дубликаты будут корректно учтены на стадии Reduce.
        final int cacheIndex = toCacheIndex(compositeKey);
        if(compositeKey == keyCache[cacheIndex]) return;
        keyCache[cacheIndex] = compositeKey;

        outputKey.set(compositeKey);
        outputValue.set(ip);
        context.write(outputKey, outputValue);
    }
}
