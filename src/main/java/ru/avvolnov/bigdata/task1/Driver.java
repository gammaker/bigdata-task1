package ru.avvolnov.bigdata.task1;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class Driver {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.set("mapred.textoutputformat.separator", ",");
        Job job = Job.getInstance(conf, "Browser Stats");

        job.setMapOutputKeyClass(LongWritable.class);
        job.setMapOutputValueClass(LongWritable.class);

        job.setInputFormatClass(TextInputFormat.class);

        job.setJarByClass(Driver.class);
        job.setMapperClass(Mapper.class);
        job.setReducerClass(Reducer.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(LongWritable.class);

        job.setGroupingComparatorClass(GroupingComparator.class);
        FileInputFormat.addInputPath(job, new Path("hdfs://localhost/user/cloudera/log1.txt"));
        FileInputFormat.addInputPath(job, new Path("hdfs://localhost/user/cloudera/log2.txt"));
        FileInputFormat.addInputPath(job, new Path("hdfs://localhost/user/cloudera/log3.txt"));
        FileInputFormat.addInputPath(job, new Path("hdfs://localhost/user/cloudera/log4.txt"));
        FileOutputFormat.setOutputPath(job, new Path("hdfs://localhost/user/cloudera/BrowserStats"));
        System.exit(job.waitForCompletion(true)? 0: 1);
    }
}
