package ru.avvolnov.bigdata.task1;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.regex.Pattern;

// Большая часть логики приложения не привязана к Hadoop, и сосредоточена в этом классе
public class MapUtils {
    private static final Pattern iePattern = Pattern.compile("(Trident|MSIE)/(\\d+)\\.");
    private static final Pattern edgePattern = Pattern.compile("Edge/(\\d+)\\.");
    private static final Pattern firefoxPattern = Pattern.compile("Firefox/(\\d+)\\.");
    private static final Pattern operaPattern = Pattern.compile("(Opera|OPR)/(\\d+)\\.");
    private static final Pattern chromePattern = Pattern.compile("Chrom(e|ium)/(\\d+)\\.");

    public static BrowserType detectBrowserByUserAgent(String str) {
        if(str == null) return BrowserType.Other;
        if(iePattern.matcher(str).find()) return BrowserType.IE;
        if(edgePattern.matcher(str).find()) return BrowserType.Edge;
        if(firefoxPattern.matcher(str).find()) return BrowserType.Firefox;
        if(operaPattern.matcher(str).find()) return BrowserType.Opera;
        if(chromePattern.matcher(str).find()) return BrowserType.Chrome;
        return BrowserType.Other;
    }

    public static InetAddress parseLineIP(String line) {
        int index = line.indexOf(" --");
        if(index == -1) return null;
        try {
            return InetAddress.getByName(line.substring(0, index));
        } catch(UnknownHostException e) {
            return null;
        }
    }

    // Переводит IP адрес в число типа long. Используется для создания уникального ключа.
    // IPv4 адреса помещаюются туда целиком.
    // IPv6 адрес будет урезан до 64 бит. Возможные случайные совпадения очень маловероятны и на статистику не повлияют.
    public static long numberFromIP(InetAddress addr) {
        long result = 0;
        for(byte b: addr.getAddress()) {
            result <<= 8;
            result |= b & 0xFF;
        }
        return result;
    }

    public static String parseLineUserAgent(String line) {
        int lastIndex = line.lastIndexOf('"');
        if(lastIndex == -1) return null;
        int firstIndex = line.lastIndexOf('"', lastIndex - 1);
        if(firstIndex == -1) return null;
        return line.substring(firstIndex + 1, lastIndex);
    }

    public static long logLineToLongIP(String line) {
        return numberFromIP(parseLineIP(line));
    }

    public static BrowserType logLineToBrowser(String line) {
        return detectBrowserByUserAgent(parseLineUserAgent(line));
    }

    public static long makeCompositeKey(long ip, int browser) {
        return (ip & ~(7L << 61)) | ((long)browser << 61);
    }

    public static int logLineToValue(String line) {
        return logLineToBrowser(line).ordinal();
    }

    public static BrowserType browserFromCompositeKey(long key) {
        return BrowserType.values()[(int)((key >> 61) & 7)];
    }
}
