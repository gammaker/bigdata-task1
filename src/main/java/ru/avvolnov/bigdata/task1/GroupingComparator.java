package ru.avvolnov.bigdata.task1;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

// Компаратор, используемый для группировки выходных данных Mapper'а по браузеру,
// где ключ составной и содержит информацию о браузере (старшие 3 бита) и IP-адресе пользователя
public class GroupingComparator extends WritableComparator {
    public GroupingComparator() {
        super(LongWritable.class, true);
    }
    @Override public int compare(WritableComparable wc1, WritableComparable wc2) {
        LongWritable pair = (LongWritable)wc1;
        LongWritable pair2 = (LongWritable)wc2;
        return new Long(pair.get() >> 61).compareTo(pair2.get() >> 61);
    }
}
