package ru.avvolnov.bigdata.task1;

import org.junit.Test;
import static org.junit.Assert.*;
import java.net.InetAddress;

public class MapUtilsTest {
    private static final String userAgentChrome =
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36";
    private static final String userAgentFirefox =
        "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0";
    private static final String userAgentEdge =
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063";
    private static final String userAgentIE =
        "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; .NET CLR 2.0.50727; .NET CLR 3.0.30729; .NET CLR 3.5.30729; InfoPath.3; LCJB; rv:11.0) like Gecko";
    private static final String userAgentOpera =
        "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36 OPR/48.0.2685.39";

    private static final String logLineUserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3163.100 Safari/537.36";
    private static final String logLine = "249.197.163.134 -- [27/Sep/2017:04:38:04 -0400] GET /page/2 HTTP/1.1\" 200 30809 \"http://www.example.com\" \"" + logLineUserAgent + "\"";

    @Test
    public void detectBrowserByUserAgent() throws Exception {
        assertEquals(BrowserType.Chrome, MapUtils.detectBrowserByUserAgent(userAgentChrome));
        assertEquals(BrowserType.Firefox, MapUtils.detectBrowserByUserAgent(userAgentFirefox));
        assertEquals(BrowserType.Edge, MapUtils.detectBrowserByUserAgent(userAgentEdge));
        assertEquals(BrowserType.IE, MapUtils.detectBrowserByUserAgent(userAgentIE));
        assertEquals(BrowserType.Opera, MapUtils.detectBrowserByUserAgent(userAgentOpera));
    }

    @Test
    public void parseLineIP() throws Exception {
        InetAddress inetAddress = MapUtils.parseLineIP(logLine);
        assertArrayEquals(
            new byte[]{(byte)249, (byte)197, (byte)163, (byte)134},
            inetAddress.getAddress()
        );
    }

    @Test
    public void numberFromIP() throws Exception {
        assertEquals(
            ((long)192 << 24) | (168 << 16) | (1 << 8) | 1,
            MapUtils.numberFromIP(InetAddress.getByName("192.168.1.1"))
        );
    }

    @Test
    public void parseLineUserAgent() throws Exception {
        assertEquals(logLineUserAgent, MapUtils.parseLineUserAgent(logLine));
    }

    @Test
    public void logLineToLongIP() throws Exception {
        assertEquals(
            ((long)249 << 24) | (197 << 16) | (163 << 8) | 134,
            MapUtils.logLineToLongIP(logLine)
        );
    }

    @Test
    public void logLineToBrowser() throws Exception {
        assertEquals(
            BrowserType.Chrome,
            MapUtils.logLineToBrowser(logLine)
        );
    }

    @Test
    public void makeCompositeKey() throws Exception {
        long ip = ((long)249 << 24) | (197 << 16) | (163 << 8) | 134;
        long compositeKey = MapUtils.makeCompositeKey(ip, BrowserType.IE.ordinal());
        assertEquals(BrowserType.IE.ordinal(), compositeKey >> 61);
        assertEquals(ip & ~(7L << 61), compositeKey & ~(7L << 61));
    }

    @Test
    public void logLineToValue() throws Exception {
        assertEquals(
                BrowserType.Chrome.ordinal(),
                MapUtils.logLineToValue(logLine)
        );
    }

    @Test
    public void browserFromCompositeKey() throws Exception {
        long ip = ((long)249 << 24) | (197 << 16) | (163 << 8) | 134;
        long compositeKey = MapUtils.makeCompositeKey(ip, BrowserType.Opera.ordinal());
        assertEquals(BrowserType.Opera, MapUtils.browserFromCompositeKey(compositeKey));
    }
}
